//
//  AllTodosVC.swift
//  ToDos
//
//  Created by reham on 4/30/18.
//  Copyright © 2018 reham. All rights reserved.
//

import UIKit
import SnapKit
import DropDown
import Toast_Swift
class AllTodosVC: UIViewController{

    lazy var todosFilterButton:UIButton = {
            var filter = UIButton()
            filter.setTitle("All Todos", for: .normal)
            filter.setTitleColor(#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 			0.3019607961, 	alpha: 1), for: .normal)
            filter.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
            filter.contentHorizontalAlignment = .center
            return filter
        }()
        lazy var todosFilterIcon:UIButton = {
            var icon = UIButton()
            icon.setImage(#imageLiteral(resourceName: "caret-down"), for: .normal)
            icon.imageEdgeInsets = UIEdgeInsetsMake(8, 4, 8, 4)
            return icon
        }()
        lazy var todosTableView:UITableView = {
            var tableView = UITableView()
            return tableView
        }()
        lazy var todosFilterView:UIView = {
            var view = UIView()
            view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            view.layer.cornerRadius = 4
            view.layer.borderWidth = 1
            view.layer.borderColor = #colorLiteral(red: 0.2549019608, green: 0.2745098039, blue: 			0.3019607843, alpha: 1)
            return view
        }()

        var dropDown = DropDown()
        var TodosFilterArray:[String] = ["All Todos","Complete","Incomplete"]
        var allTodosArray :[TodoResponse] = []
        var completedArray :[TodoResponse] = []
        var inCompletedArray :[TodoResponse] = []
        var todosArray :[TodoResponse] = []
        let cellIdentifier = "TodoCell"
        let provider = TodoNetworkProvider()

        override func viewDidLoad() {
            super.viewDidLoad()
            setupView()
            setConstrains()
            getTodosFromAPI()
            
        }
        func setConstrains(){
            let superView = self.view
            
            todosFilterView.addSubview(todosFilterButton)
            todosFilterView.addSubview(todosFilterIcon)
            
            superView?.addSubview(todosFilterView)
            superView?.addSubview(todosTableView)
            
            todosFilterView.snp.makeConstraints { (make) in
                make.left.equalTo(8)
                make.right.equalTo(-8)
		make.top.equalToSuperview().offset(70)
                make.height.equalTo(50)
            }
            todosFilterIcon.snp.makeConstraints { (make) in
		make.right.equalToSuperview().offset(-8)
		make.top.equalToSuperview()               
		make.width.equalTo(30)
                make.height.equalToSuperview()  
            }
            todosFilterButton.snp.makeConstraints { (make) in
                make.left.equalToSuperview()
		make.right.equalTo(todosFilterIcon.snp.left)
                make.top.equalToSuperview()
                make.height.equalToSuperview()
            }
            todosTableView.snp.makeConstraints { (make) in
                make.left.equalTo(8)
                make.right.equalTo(-8)
                make.top.equalTo(todosFilterView.snp.bottom).offset(4)
                make.bottom.equalToSuperview().offset(-4)
            }
        }
    }
extension AllTodosVC{

        func setupView() {

            self.title = "Todos"
            self.view.backgroundColor = #colorLiteral(red: 0.895696938, green: 0.8903717399, blue: 			0.8997904062, alpha: 1)

            self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 			0, alpha: 1)
            self.navigationController?.navigationBar.titleTextAttributes  = 					[NSAttributedStringKey.foregroundColor:UIColor.white]

            todosFilterButton.addTarget(self, action: #selector(todosStatusAction), for: .touchUpInside)
            todosFilterIcon.addTarget(self, action: #selector(todosStatusAction), for: .touchUpInside)
            
            todosTableView.register(TodoCell.self, forCellReuseIdentifier: cellIdentifier)
            todosTableView.dataSource = self
            todosTableView.delegate = self
            
        }
         func getTodosFromAPI() {
            self.view.makeToastActivity(.center)
            provider.getAllTodos(onSuccess: { [unowned self] (response, statusCode) in
                self.view.hideToastActivity()
                self.todosArray = response
                self.allTodosArray = self.todosArray
                self.filterTodos()
                self.todosTableView.reloadData()
            }) { [unowned self] (error, statusCode) in
                self.view.hideToastActivity()
                self.view.makeToast("Something go wrong")
            }
        }
 	func filterTodos() {
            for item in todosArray {
                if item.completed{
                    completedArray.append(item)
                }else{
                    inCompletedArray.append(item)
                }
            }
        }
        @objc func todosStatusAction() {
            dropDown.anchorView = todosFilterButton
            dropDown.bottomOffset = CGPoint(x: 30, y:(dropDown.anchorView?.plainView.bounds.height)!)
            dropDown.dataSource = TodosFilterArray
            dropDown.customCellConfiguration = { (index: Int, item: String, cell:DropDownCell) -> Void in
                cell.optionLabel.textAlignment = .center
                cell.optionLabel.font = UIFont.boldSystemFont(ofSize: 20)
                cell.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.todosFilterButton.setTitle(item, for: .normal)
                self.checkSelectedStatus(status: item)
            }
            dropDown.show()
        }
        func checkSelectedStatus(status:String) {
            if status == "All Todos"{
                todosArray = allTodosArray
            }else if status == "Complete"{
                todosArray = completedArray
            }else{
                todosArray = inCompletedArray
            }
            todosTableView.reloadData()
        }
       
    }
extension AllTodosVC:UITableViewDataSource{
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return todosArray.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! TodoCell
            cell.setCell(todo: todosArray[indexPath.row])
            cell.selectionStyle = .none
            return cell
        }
        
    }
extension AllTodosVC:UITableViewDelegate{
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 50
        }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "TodoDetailsVC") as! TodoDetailsVC
            vc.todoObject = todosArray[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
  
}


