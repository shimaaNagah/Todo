//
//  TodoDetailsVC.swift
//  ToDos
//
//  Created by reham on 4/30/18.
//  Copyright © 2018 reham. All rights reserved.
//

import UIKit
import Toast_Swift

class TodoDetailsVC: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var todoTitleLabel: UILabel!
    
    @IBOutlet weak var statusImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userEmailLabel: UILabel!
    
    @IBOutlet weak var userPhoneLabel: UILabel!
    var todoObject:TodoResponse!
    let provider = UserNetworkProvider()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        getUserDetailsFromAPI()
    }
    func setupView(){
        todoTitleLabel.text = todoObject.title
        if (todoObject?.completed)!{
            statusImageView.image = #imageLiteral(resourceName: "checked")
        }else{
            statusImageView.image = #imageLiteral(resourceName: "cancel")
        }
        
        containerView.layer.cornerRadius = 4
        containerView.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        containerView.layer.borderWidth = 1
        
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
    func getUserDetailsFromAPI(){
        self.view.makeToastActivity(.center)
        provider.getUserDetails(userID: todoObject.userId, onSuccess: {[unowned self] (response, statusCode) in
            self.view.hideToastActivity()
            self.userNameLabel.text =  response.name
            self.userEmailLabel.text = response.email
            self.userPhoneLabel.text = response.phone
        }) { [unowned self](error, statusCode) in
            self.view.hideToastActivity()
            self.view.makeToast("Something go wrong")
        }
    }
}




