//
//  UserNetworkProvider.swift
//  ToDos
//
//  Created by reham on 4/30/18.
//  Copyright © 2018 reham. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
class UserNetworkProvider {
    typealias successCompletionHandler = (_ response:UserResponse,_ statusCode:Int)-> Void
    typealias errorCompletionHandler = (_ error:Error,_ statusCode:Int)-> Void
    func getUserDetails(userID:Int,onSuccess:@escaping 		
	    	successCompletionHandler ,onFailure:@escaping errorCompletionHandler) {
        let url = Constants.baseURL+"/users/\(userID)"
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, 
	headers: Headers.defaultHeader).responseJSON { (response) in
            if response.result.isSuccess
            {
                let userResponse = UserResponse(fromJson:JSON(response.result.value!))
                onSuccess(userResponse,(response.response?.statusCode)!)
            }
            else
            {
                onFailure(response.error!,(response.response?.statusCode)!)
            }
        }
    }
}
