//
//  TodoNetworkProvider.swift
//  ToDos
//
//  Created by reham on 4/30/18.
//  Copyright © 2018 reham. All rights reserved.
//

import Foundation
import Moya
import Moya_Gloss
class TodoNetworkProvider {
    var provider = MoyaProvider<TodoService>()
    typealias successCompletionHandler = (_ response:[TodoResponse],_ statusCode:Int)->Void
    typealias errorCompletionHandler = (_ error:MoyaError,_ statusCode:Int)->Void
    init() {
        let endpointClosure = { (target: TodoService) -> Endpoint<TodoService> in
            let defaultEndpoint = MoyaProvider.defaultEndpointMapping(for: target)
            
            return defaultEndpoint.adding(newHTTPHeaderFields:Headers.defaultHeader)
        }
        provider = MoyaProvider<TodoService>(endpointClosure: endpointClosure)
    }
    
    func getAllTodos(onSuccess:@escaping successCompletionHandler,onFailure:@escaping errorCompletionHandler) {
        provider.request(.allTodos()) { (result) in
            switch result{
            case .success(let response):
                do{
                    let todoResponse = try response.mapArray(TodoResponse.self)
                    onSuccess(todoResponse, response.statusCode)
                }catch{
                    
                }
            case .failure(let error):
                    onFailure(error,(result.value?.statusCode)!)
            }
        }
    }
}
