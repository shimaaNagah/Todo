//
//	Addres.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class Addres : NSObject, NSCoding{

	var city : String!
	var geo : Geo!
	var street : String!
	var suite : String!
	var zipcode : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		city = json["city"].stringValue
		let geoJson = json["geo"]
		if !geoJson.isEmpty{
			geo = Geo(fromJson: geoJson)
		}
		street = json["street"].stringValue
		suite = json["suite"].stringValue
		zipcode = json["zipcode"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
        var dictionary = [String:Any]()
		if city != nil{
			dictionary["city"] = city
		}
		if geo != nil{
			dictionary["geo"] = geo.toDictionary()
		}
		if street != nil{
			dictionary["street"] = street
		}
		if suite != nil{
			dictionary["suite"] = suite
		}
		if zipcode != nil{
			dictionary["zipcode"] = zipcode
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         city = aDecoder.decodeObject(forKey: "city") as? String
         geo = aDecoder.decodeObject(forKey: "geo") as? Geo
         street = aDecoder.decodeObject(forKey: "street") as? String
         suite = aDecoder.decodeObject(forKey: "suite") as? String
         zipcode = aDecoder.decodeObject(forKey: "zipcode") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if city != nil{
			aCoder.encode(city, forKey: "city")
		}
		if geo != nil{
			aCoder.encode(geo, forKey: "geo")
		}
		if street != nil{
			aCoder.encode(street, forKey: "street")
		}
		if suite != nil{
			aCoder.encode(suite, forKey: "suite")
		}
		if zipcode != nil{
			aCoder.encode(zipcode, forKey: "zipcode")
		}

	}

}
