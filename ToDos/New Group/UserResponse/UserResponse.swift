//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class UserResponse : NSObject, NSCoding{

	var address : Addres!
	var company : Company!
	var email : String!
	var id : Int!
	var name : String!
	var phone : String!
	var username : String!
	var website : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		let addressJson = json["address"]
		if !addressJson.isEmpty{
			address = Addres(fromJson: addressJson)
		}
		let companyJson = json["company"]
		if !companyJson.isEmpty{
			company = Company(fromJson: companyJson)
		}
		email = json["email"].stringValue
		id = json["id"].intValue
		name = json["name"].stringValue
		phone = json["phone"].stringValue
		username = json["username"].stringValue
		website = json["website"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
        var dictionary = [String:Any]()
		if address != nil{
			dictionary["address"] = address.toDictionary()
		}
		if company != nil{
			dictionary["company"] = company.toDictionary()
		}
		if email != nil{
			dictionary["email"] = email
		}
		if id != nil{
			dictionary["id"] = id
		}
		if name != nil{
			dictionary["name"] = name
		}
		if phone != nil{
			dictionary["phone"] = phone
		}
		if username != nil{
			dictionary["username"] = username
		}
		if website != nil{
			dictionary["website"] = website
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObject(forKey: "address") as? Addres
         company = aDecoder.decodeObject(forKey: "company") as? Company
         email = aDecoder.decodeObject(forKey: "email") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int
         name = aDecoder.decodeObject(forKey: "name") as? String
         phone = aDecoder.decodeObject(forKey: "phone") as? String
         username = aDecoder.decodeObject(forKey: "username") as? String
         website = aDecoder.decodeObject(forKey: "website") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if company != nil{
			aCoder.encode(company, forKey: "company")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if phone != nil{
			aCoder.encode(phone, forKey: "phone")
		}
		if username != nil{
			aCoder.encode(username, forKey: "username")
		}
		if website != nil{
			aCoder.encode(website, forKey: "website")
		}

	}

}
