//
//	Company.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class Company : NSObject, NSCoding{

	var bs : String!
	var catchPhrase : String!
	var name : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		bs = json["bs"].stringValue
		catchPhrase = json["catchPhrase"].stringValue
		name = json["name"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
        var dictionary = [String:Any]()
		if bs != nil{
			dictionary["bs"] = bs
		}
		if catchPhrase != nil{
			dictionary["catchPhrase"] = catchPhrase
		}
		if name != nil{
			dictionary["name"] = name
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         bs = aDecoder.decodeObject(forKey: "bs") as? String
         catchPhrase = aDecoder.decodeObject(forKey: "catchPhrase") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if bs != nil{
			aCoder.encode(bs, forKey: "bs")
		}
		if catchPhrase != nil{
			aCoder.encode(catchPhrase, forKey: "catchPhrase")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}

	}

}
