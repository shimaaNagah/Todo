//
//	TodoResponse.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

//	The "Swift - Struct - Gloss" support has been made available by CodeEagle
//	More about him/her can be found at his/her website: https://github.com/CodeEagle

import Foundation 
import Gloss

//MARK: - TodoResponse
public struct TodoResponse: Glossy {

	public let completed : Bool!
	public let id : Int!
	public let title : String!
	public let userId : Int!


	//MARK: Decodable
	public init?(json: JSON){
		completed = "completed" <~~ json
		id = "id" <~~ json
		title = "title" <~~ json
		userId = "userId" <~~ json
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
		"completed" ~~> completed,
		"id" ~~> id,
		"title" ~~> title,
		"userId" ~~> userId,
		])
	}

}
