//
//  TodoService.swift
//  ToDos
//
//  Created by reham on 4/30/18.
//  Copyright © 2018 reham. All rights reserved.
//

import Foundation
import Moya
enum TodoService{
    case allTodos()
}
extension TodoService:TargetType{
    var baseURL: URL {
        return URL(string:Constants.baseURL)!
    }
    
    var path: String {
        return "/todos"
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var headers: [String : String]? {
        return Headers.defaultHeader
    }
    
    
}
