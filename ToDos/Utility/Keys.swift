//
//  Keys.swift
//  ToDos
//
//  Created by reham on 4/30/18.
//  Copyright © 2018 reham. All rights reserved.
//

import Foundation
class Constants {
    static let baseURL = "https://jsonplaceholder.typicode.com"
}
class Headers {
    static let defaultHeader = ["Content-type":"application/json"]
}
