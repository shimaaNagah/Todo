//
//  TodoCell.swift
//  ToDos
//
//  Created by reham on 4/30/18.
//  Copyright © 2018 reham. All rights reserved.
//

import UIKit
import SnapKit
class TodoCell: UITableViewCell {

    lazy var todoView:UIView = {
        var view = UIView()
        view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        view.layer.cornerRadius = 5
        return view
    }()
    lazy var titleLabel:UILabel = {
        var title = UILabel()
        title.text = "title"
        title.font = UIFont.boldSystemFont(ofSize: 14)
        return title
    }()
    lazy var statusImageView:UIImageView = {
        var image = UIImageView()
        image.contentMode = .scaleAspectFit
        return image
    }()
    override func awakeFromNib() {
        super.awakeFromNib() 
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.backgroundColor = #colorLiteral(red: 0.895696938, green: 0.8903717399, blue: 0.8997904062, alpha: 1)
        self.setConstrains()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setConstrains(){
        
        todoView.addSubview(titleLabel)
        todoView.addSubview(statusImageView)
        
        self.addSubview(todoView)
        
        todoView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-4)
        }
        statusImageView.snp.makeConstraints { (make) in
	    make.right.equalToSuperview().offset(-8)
	    make.width.equalTo(statusImageView.snp.height)
            make.top.equalToSuperview().offset(8)
            make.bottom.equalToSuperview().offset(-8)  
        }
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(8)
	    make.right.equalTo(statusImageView.snp.left).offset(-8)            
	    make.top.equalToSuperview()
            make.height.equalToSuperview()  
        }  
    }
    func setCell(todo:TodoResponse)  {
        titleLabel.text = todo.title
        if todo.completed{
            statusImageView.image = #imageLiteral(resourceName: "checked")
        }else{
            statusImageView.image = #imageLiteral(resourceName: "cancel")
        }
    }
}

